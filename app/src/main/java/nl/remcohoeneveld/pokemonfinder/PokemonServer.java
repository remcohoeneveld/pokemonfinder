package nl.remcohoeneveld.pokemonfinder;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PokemonServer extends AsyncTask<Void, Void, List<String>> {


    private WeakReference<MainActivity> mainActivityWeakReference;
    private CurrentLocation currentLocation;
    public double longitude;
    public double latitude;
    public String lon;
    public String lat;
    public String number;

    public List<String> pokemon;

    public PokemonServer(MainActivity mainActivity){

        // Creates reference to mainActivity
        mainActivityWeakReference = new WeakReference<MainActivity>(mainActivity);

        // Gets the currentLocation from the mainActivity
        currentLocation = new CurrentLocation(mainActivity);

    }

    @Override
    protected List<String> doInBackground(Void... params) {

        // Check for the currentLocation
        if(currentLocation.canGetLocation()){

            longitude = currentLocation.getLongitude();
            latitude = currentLocation.getLatitude();

            //Sets location to string so it can be LOGGED
            lon = Double.toString(longitude);
            lat = Double.toString(latitude);

            //Round up the lattitude so it can be parsed to the base URL
            double finalValue = Math.round(latitude);
            DecimalFormat df = new DecimalFormat("###.#");
            number = df.format(finalValue);

            Log.d("LONGTITUDE", lon);
            Log.d("LATITUDE", lat);

        } else {
            int min = 1;
            int max = 100;

            Random r = new Random();
            int i1 = r.nextInt(max - min + 1) + min;
            number = Integer.toString(i1);


        }

        // set all the values when there is no internet
        String name = "Sorry, we konden hier geen pokemon vinden";
        String sprite = "";
        String type = "Geen";

        HttpURLConnection con = null;

        try {
            //getting the pokemon datase url [ Number = the rounded number of latitude ]
            //generating different pokemons for different areas.
            if (number != null){
            URL baseUrl = new URL(
                    "http://pokeapi.co/api/v2/pokemon/"+ number +"/");
            con = (HttpURLConnection) baseUrl.openConnection();
            //setting the connection Timout and Request methods
            } else {
                URL baseUrl = new URL(
                        "http://pokeapi.co/api/v2/pokemon/"+ 22 +"/");
                con = (HttpURLConnection) baseUrl.openConnection();
            }
            // /* milliseconds */
            con.setReadTimeout(10000);
            con.setConnectTimeout(15000);

            //getting the request
            con.setRequestMethod("GET");

            //setting the do input
            con.setDoInput(true);

            // Start the query
            con.connect();

            // Read results from the query
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(con.getInputStream(), "UTF-8"));
            String payload = reader.readLine();
            reader.close();

            // Parse to get translated text
            JSONObject jsonObject = new JSONObject(payload);

            name = jsonObject.getString("name");
            sprite = jsonObject.getJSONObject("sprites").getString("front_default");
            type = jsonObject.getJSONArray("types").getJSONObject(0).getJSONObject("type").getString("name");

            //creating a new ArrayList with type of string
            pokemon = new ArrayList<String>();
            pokemon.add(name);
            pokemon.add(sprite);
            pokemon.add(type);


        } catch (IOException e) {
            //creating a new exception
            Log.e(MainActivity.TAG, "IOException", e);
        } catch (JSONException e) {
            Log.e(MainActivity.TAG, "JSONException", e);
        } catch (Exception e) {
            Log.d(MainActivity.TAG, "Pokemons not incomming... ", e);
        } finally {
            if (con != null) {
                con.disconnect();
            }
        }

        Log.d(MainActivity.TAG, "return ==" + name);
        Log.d(MainActivity.TAG, "return ==" + sprite);

        return pokemon;
    }

    @Override
    protected void onPostExecute(List<String> pokemons){

        // the result goes to the main activity
        mainActivityWeakReference.get().setPokemon(pokemons);
    }

}
