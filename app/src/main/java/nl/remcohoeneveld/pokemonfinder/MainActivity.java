package nl.remcohoeneveld.pokemonfinder;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class MainActivity extends Activity {

    public final static String TAG = "PokemonFinder";
    double longitude;
    double latitude;
    public String pokemonName;
    public String pokemonImage;
    public String pokemonType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first

        String savedPokemon = getPref("PokemonName", getApplicationContext());

        if (savedPokemon != null){
            TextView tv_savedPokemon = (TextView) findViewById(R.id.textView_savedPokemon);
            tv_savedPokemon.setText("Last pokemon found " + savedPokemon);
        }

    }

    public void getPokemon(View v){

        //getting all the pokemons from the database
        PokemonServer server = new PokemonServer(this);
        server.execute();

    }

    public static String getPref(String key, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key, null);
    }

    public void setPokemon(List<String> pokemon){

        //getting the pokemon current location
        CurrentLocation currentLocation = new CurrentLocation(MainActivity.this);

        if(currentLocation.canGetLocation()){
            //getting latitude and longtitude from currentlocation
            longitude = currentLocation.getLongitude();
            latitude = currentLocation.getLatitude();
        } else {
            //show the error alert for the long and latitude
            currentLocation.showSettingsAlert();
        }
        //creating the intent to the pokemon view
        Intent intent = new Intent(MainActivity.this, PokemonView.class);
        //getting the pokemon values from the List
        pokemonName = pokemon.get(0);
        pokemonImage = pokemon.get(1);
        pokemonType = pokemon.get(2);

        //starting the intent with all the values for pokemon
        intent.putExtra("pokemonName", pokemonName);
        intent.putExtra("pokemonImage", pokemonImage);
        intent.putExtra("pokemonType", pokemonType);

        //add the long and lat for the GPS map marker
        intent.putExtra("longitude", longitude);
        intent.putExtra("latitude", latitude);

        startActivity(intent);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent menuIntent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(menuIntent);
        }

        return super.onOptionsItemSelected(item);
    }


}
