package nl.remcohoeneveld.pokemonfinder;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class PokemonView extends Activity {

    public Double lon;
    public Double lat;

    public String pokemonName;
    public String pokemonImage;
    public String pokemonType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon_view);

        //getting a bundle from intent extra's
        Bundle bundle = getIntent().getExtras();

        //getting all the pokemon values
        pokemonName = bundle.getString("pokemonName");
        pokemonImage = bundle.getString("pokemonImage");
        pokemonType = bundle.getString("pokemonType");

        //getting the long and latitude to send to map marker
        lon = bundle.getDouble("longitude");
        lat = bundle.getDouble("latitude");

        //setting the textview for the pokemon name
        TextView tv_pokemonName = (TextView) findViewById(R.id.textView_pokemon);
        tv_pokemonName.setText(pokemonName);

        //setting the textview for the pokemon type

        TextView tv_pokemonType = (TextView) findViewById(R.id.textView_pokemon_type);
        tv_pokemonType.setText("PokemonType: " + pokemonType);


        //creating a new downloadImage task for the pokemonImage and adding the image
        new DownloadImageTask((ImageView) findViewById(R.id.imageView_pokemon_image)).execute(pokemonImage);

    }

    public void setMaps(View v){

        Intent intentMaps = new Intent(PokemonView.this, MapsActivity.class);

        //setting the lon and lat for the map
        intentMaps.putExtra("longitude",    lon);
        intentMaps.putExtra("latitude",     lat);

        //setting the pokemonName, type, image;
        intentMaps.putExtra("pokemonName",  pokemonName);
        intentMaps.putExtra("pokemonImage", pokemonImage);
        intentMaps.putExtra("pokemonType",  pokemonType);

        //starting the new intent to maps
        startActivity(intentMaps);
    }
}
