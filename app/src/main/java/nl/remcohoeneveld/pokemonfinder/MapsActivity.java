package nl.remcohoeneveld.pokemonfinder;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    public Double longitude;
    public Double latitude;

    public String pokemonName;
    public String pokemonImage;
    public String pokemonType;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Bundle bundle = getIntent().getExtras();

        longitude = bundle.getDouble("longitude");
        latitude = bundle.getDouble("latitude");

        pokemonName = bundle.getString("pokemonName");
        pokemonImage = bundle.getString("pokemonImage");
        pokemonType = bundle.getString("pokemonType");

    }

    public static void putPref(String key, String value, Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.apply();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        //get the marker
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
                R.drawable.pokeball);

        //resize the marker
        Bitmap bitmapResize = Bitmap.createScaledBitmap(bitmap, 80, 80, false);

        // Add a marker of the current location where the pokemon is found
        final LatLng pokemon = new LatLng(longitude, latitude);


        //create a marker
        final Marker currentPokemon = mMap.addMarker(new MarkerOptions()
                .draggable(true)
                .position(pokemon)
                .title(pokemonName)
                .snippet(pokemonType + " pokemon")
                .icon(BitmapDescriptorFactory.fromBitmap(bitmapResize))
                .draggable(true)
        );

        //create a on window click listener
        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener(){
            @Override
            public void onInfoWindowClick(Marker marker){
                if(marker.getTitle().equals(pokemonName)){

                    Context context = getApplicationContext();
                    putPref("PokemonName", pokemonName, context);

                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(context, pokemonName + " is added to your recent found pokemon", duration);
                    toast.show();
                }
            }
        });

        //sets the CameraPosition of the map
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(pokemon)            // Sets the center of the map to Mountain View
                .zoom(10)                    // Sets the zoom
                .bearing(10)                // Sets the orientation of the camera to east
                .tilt(40)                   // Sets the tilt of the camera to 40 degrees
                .build();                   // Creates a CameraPosition from the builder

        //Animates the camera in with a CameraUpdateFactory
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }



}
