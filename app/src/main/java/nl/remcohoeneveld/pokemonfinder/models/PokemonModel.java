package nl.remcohoeneveld.pokemonfinder.models;

public class PokemonModel {
    private String name;

    public String getName(){
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
